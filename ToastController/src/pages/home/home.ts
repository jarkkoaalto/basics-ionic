import { Component } from '@angular/core';
import { NavController,  LoadingController, AlertController, 
  ToastController, ActionSheetController, ModalController, PopoverController} from 'ionic-angular';
import { OfficePage } from '../office/office';
import { ProfilePage} from '../profile/profile';
import { PopoverPage } from '../popover/popover';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'

})
export class HomePage {

  username: string  = "";
  password: string  = "";
  userType: any;
  gender: any;
  notifigation: boolean;

  constructor(public navCtrl: NavController, public LoadingController: LoadingController, 
    public AlertController: AlertController, public ToastController: ToastController,
    public ActionSheetController: ActionSheetController, public ModalController: ModalController,
    public PopoverController: PopoverController ){

  }

  login(){
    let loading = this.LoadingController.create({
      content: "Loading ...",
      spinner: "ios"
    });
    loading.present();
  
    setTimeout(()=> {
      loading.dismiss();
    }, 3000);

  }
  cancel(){
    console.log("User clicked Cancel");

    this.username="";
    this.password="";
  }


  alert(){
  //  let alert = this.AlertController.create({
  //    title: "Concretulations",
  //    subTitle:"You have won",
  //    message: "Enter your email address",
  //     inputs:[{
  //       name: "email",
  //       placeholder:"user@email.com"
  //     }],
  //     buttons: [{
  //     text: 'OK',
  //      handler:(data) => {console.log(data)}
  //   }]
  //   });

    let alert = this.AlertController.create();
    alert.setTitle("Congratulrations");
   
    alert.addInput({
      type: 'checkbox',
      label: 'Send me the gift',
      value: 'send'
    });

    alert.addInput({
      type: 'checkbox',
      label: 'No thank You',
      value: 'dontSend'
    });
    alert.addButton({
      text: 'Ok',
      handler:(data)=>{
        console.log(data);
      }
    })
   
    alert.present();
  }

  toast(){
    let toast = this.ToastController.create({
      message: "This is a toast",
      showCloseButton: true,
      closeButtonText: "OK",
      position: "top"

    }) 
    toast.present();
  }


  actionsheet(){

    let action = this.ActionSheetController.create({
      title: "Select an option",
      buttons:[
        {
          text: "Share with Facebook",
          icon: "logo-facebook",
          handler: () => {
            console.log("Shared via Facebook");
          }
        },
        {
          text: "Share with Email",
          icon: "mail",
          handler: () => {
            console.log("Shared via Email");
            return false;
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          icon: "close",
          handler: () => {
            console.log("Chancelled");
          }
        }
    ]
    })

    action.present();
  }

  gotoOffice(){
    this.navCtrl.push(OfficePage,{"username": this.username, "password":this.password});
  }

  openModal(){
 let modal =  this.ModalController.create(ProfilePage,{"username":this.username});
 modal.onDidDismiss((data)=> {
   console.log(data);
 })
 modal.present();
  }

  preserntPopover(myEvent){
   let popover = this.PopoverController.create(PopoverPage);
   popover.present({
     ev: myEvent
   });
  }
}