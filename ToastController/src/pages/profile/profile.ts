import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';


@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public ViewController:ViewController) {
    console.log(this.navParams.get("username"))
  }

  close(){
    this.ViewController.dismiss({"status":"done"});
  }
}
