import { Component } from '@angular/core';
import { NavController,  LoadingController, AlertController} from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  username: string  = "";
  password: string  = "";
  userType: any;
  gender: any;
  notifigation: boolean;

  constructor(public navCtrl: NavController, public LoadingController: LoadingController, public AlertController: AlertController) {

  }

  login(){
    let loading = this.LoadingController.create({
      content: "Loading ...",
      spinner: "ios"
    });
    loading.present();
  
    setTimeout(()=> {
      loading.dismiss();
    }, 3000);

  }
  cancel(){
    console.log("User clicked Cancel");

    this.username="";
    this.password="";
  }


  alert(){
  //  let alert = this.AlertController.create({
  //    title: "Concretulations",
  //    subTitle:"You have won",
  //    message: "Enter your email address",
  //     inputs:[{
  //       name: "email",
  //       placeholder:"user@email.com"
  //     }],
  //     buttons: [{
  //     text: 'OK',
  //      handler:(data) => {console.log(data)}
  //   }]
  //   });

    let alert = this.AlertController.create();
    alert.setTitle("Congratulrations");
   
    alert.addInput({
      type: 'checkbox',
      label: 'Send me the gift',
      value: 'send'
    });

    alert.addInput({
      type: 'checkbox',
      label: 'No thank You',
      value: 'dontSend'
    });
    alert.addButton({
      text: 'Ok',
      handler:(data)=>{
        console.log(data);
      }
    })
   
    alert.present();
  }

}
