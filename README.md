# Basics-ionic

About this course
Learn to build mobile apps for Android and iOS using Web Technologies.

Ionic Framework is one of the most popular frameworks for cross-platform mobile app development. It lets you build truly hybrid mobile apps for both Android and iOS using web technologies like HTML, SCSS and TypeScript. This course covers the latest version of Ionic, Ionic Framework v3 which uses Angular as the backing framework.

This course is an essential for Web Developers, Professionals and Students who wish to develop Mobile Apps using the web development skills they have (HTML, SCSS and TypeScript).