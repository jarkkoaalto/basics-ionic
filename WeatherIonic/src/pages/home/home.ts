import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  weatherInfo: any = { };
  cityName: string = "Jyväskylä";

  constructor(public navCtrl: NavController, public Http: Http
  ,public LoadingController: LoadingController) {

    this.getData();
  }

  getData(){
    let loading = this.LoadingController.create()
    loading.present();
    this.Http.get("http://api.openweathermap.org/data/2.5/weather?q=" + this.cityName + "&appId=6082d24c5d769b7f197c93695ca855bb").subscribe((data) => {
      console.log(data.json())
      this.weatherInfo = data.json();
    },(error) => {
      console.log(error);
      loading.dismiss();
    })
  }
}
